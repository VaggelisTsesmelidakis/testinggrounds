// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Engine/World.h" // could have been omitted, for IntelliSense purposes only
#include "Math/UnrealMathUtility.h"
#include "ActorPool.h"
#include "NavigationSystem.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	NavigationBoundsOffset = FVector(2000, 0, 0);

	MinExtent = FVector(0, -2000, 0);
	MaxExtent = FVector(4000, 2000, 0);
}

void ATile::SetPool(UActorPool* InPool)
{
	UE_LOG(LogTemp, Warning, TEXT("[%s] Setting Pool %s"), *(this->GetName()), *(InPool->GetName()));
	Pool = InPool;

	PositionNavMeshBoundsVolume();
}

void ATile::PositionNavMeshBoundsVolume()
{
	NavMeshBoundsVolume = Pool->CheckOut();
	if (NavMeshBoundsVolume == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[%s] Not enough actors in pool."), *GetName());
		return;
	}
	UE_LOG(LogTemp, Warning, TEXT("[%s] Checked out: {%s}"), *GetName(), *NavMeshBoundsVolume->GetName());
	NavMeshBoundsVolume->SetActorLocation(GetActorLocation() + NavigationBoundsOffset);
	FNavigationSystem::Build(*GetWorld());
}


void ATile::PlaceActors(TSubclassOf<AActor> ToSpawn, FSpawnParams SpawnParams, FSpawnTransform SpawnTransform)
{
	RandomlyPlaceActors(ToSpawn, SpawnParams, SpawnTransform);
}

void ATile::PlaceAIPawns(TSubclassOf<APawn> ToSpawn, FSpawnParams SpawnParams, FSpawnTransform SpawnTransform)
{
	RandomlyPlaceActors(ToSpawn, SpawnParams, SpawnTransform);
}

bool ATile::FindEmptyLocation(FVector& OutLocation, float Radius) {
	FBox Bounds(MinExtent, MaxExtent);
	const int MAX_ATTEMPTS = 100;
	for (size_t i = 0; i < MAX_ATTEMPTS; i++)
	{
		FVector CandidatePoint = FMath::RandPointInBox(Bounds);
		if (CanSpawnAtLocation(CandidatePoint, Radius)) {
			OutLocation = CandidatePoint;
			return true;
		}
	}
	return false;
}

void ATile::PlaceActor(TSubclassOf<AActor> ToSpawn, FSpawnTransform& SpawnTransform) {
	AActor* Spawned = GetWorld()->SpawnActor<AActor>(ToSpawn);
	Spawned->SetActorRelativeLocation(SpawnTransform.Location);
	Spawned->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	Spawned->SetActorRotation(FRotator(0, SpawnTransform.Rotation, 0));
	Spawned->SetActorScale3D(FVector(SpawnTransform.Scale));
}

void ATile::PlaceActor(TSubclassOf<APawn> ToSpawn, FSpawnTransform& SpawnTransform)
{
	/*Experienced some nullptr crashes here because spawns at the origin (before relocation) were failing
	  due to collision. Therefore I created a custom FActorSpawnParamters struct with an "AlwaysSpawn" parameter*/
	FActorSpawnParameters AISpawnParams;
	AISpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;


	APawn* Spawned = GetWorld()->SpawnActor<APawn>(ToSpawn, AISpawnParams);
	/*Debugging was necessary here after nullptr errors (AI mannequins were not spawning due to collisions*/
	if (!Spawned)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not spawn AIPawn!!!"))
		return;
	}
	Spawned->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	Spawned->SetActorRelativeLocation(SpawnTransform.Location);
	Spawned->SetActorRotation(FRotator(0, SpawnTransform.Rotation, 0));
	Spawned->SetActorScale3D(FVector(SpawnTransform.Scale));
	Spawned->SpawnDefaultController();
}

template<class T>
void ATile::RandomlyPlaceActors(TSubclassOf<T> ToSpawn, FSpawnParams& SpawnParams, FSpawnTransform& SpawnTransform)
{
	int NumberToSpawn = FMath::RandRange(SpawnParams.MinSpawn, SpawnParams.MaxSpawn);

	for (int i = 0; i < NumberToSpawn; ++i)
	{
		FSpawnTransform SpawnTransform;
		SpawnTransform.Scale = FMath::RandRange(SpawnParams.MinScale, SpawnParams.MaxScale);
		bool found = FindEmptyLocation(SpawnTransform.Location, SpawnParams.Radius * SpawnTransform.Scale);
		if (found) {

			SpawnTransform.Rotation = FMath::RandRange(-180.f, 180.f);
			PlaceActor(ToSpawn, SpawnTransform);
		}
	}
}


// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
}

void ATile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Pool->CheckIn(NavMeshBoundsVolume);
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ATile::CanSpawnAtLocation(FVector Location, float Radius)
{
	FHitResult HitResult;
	FVector GlobalLocation = ActorToWorld().TransformPosition(Location);
	bool HasHit = GetWorld()->SweepSingleByChannel(
		HitResult,
		GlobalLocation,
		GlobalLocation + FVector(0, 0, 0.1f),
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeSphere(Radius)
	);
	return !HasHit;
}