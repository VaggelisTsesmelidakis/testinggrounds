// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

USTRUCT(BlueprintType)
struct FSpawnTransform
{
	GENERATED_USTRUCT_BODY()

	FVector Location;
	float Rotation;
	float Scale;
};

USTRUCT(BlueprintType)
struct FSpawnParams
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	int MinSpawn = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	int MaxSpawn = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	float Radius = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	float MinScale = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	float MaxScale = 1;
};

// Forward declerations
class UActorPool;

UCLASS()
class TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	/* Populate the scene with a certain number of various props*/
	UFUNCTION(BlueprintCallable, Category = "Spawning")
	void PlaceActors(TSubclassOf<AActor> ToSpawn, FSpawnParams SpawnParams, FSpawnTransform SpawnTransform);

	/** Populate the scene with AI characters*/
	UFUNCTION(BlueprintCallable, Category = "Spawning")
	void PlaceAIPawns(TSubclassOf<APawn> ToSpawn, FSpawnParams SpawnParams, FSpawnTransform SpawnTransform);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category = "Navigation")
	FVector NavigationBoundsOffset;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MinExtent;
	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MaxExtent;

	//UPROPERTY(EditAnywhere, Category = "Spawning")
	//FSpawnParams SpawnParameters;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Pool")
	void SetPool(UActorPool* Pool);

private:

	void PositionNavMeshBoundsVolume();

	bool FindEmptyLocation(FVector& OutLocation, float Radius);

	template<class T>
	void RandomlyPlaceActors(TSubclassOf<T> ToSpawn, FSpawnParams& SpawnParams, FSpawnTransform& SpawnTransform);

	void PlaceActor(TSubclassOf<AActor> ToSpawn, FSpawnTransform& SpawnTransform);

	void PlaceActor(TSubclassOf<APawn> ToSpawn, FSpawnTransform& SpawnTransform);

	bool CanSpawnAtLocation(FVector Location, float Radius);

	UActorPool* Pool;

	AActor* NavMeshBoundsVolume;
};
