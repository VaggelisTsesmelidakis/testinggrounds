// Fill out your copyright notice in the Description page of Project Settings.


#include "InfiniteTerrainGameMode.h"
#include "NavMeshBoundsVolume.h"
#include "EngineUtils.h"
#include "ActorPool.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode()
{
	NavMeshBoundsVolumePool = CreateDefaultSubobject<UActorPool>(FName("NavMeshBoundsVolume Pool"));
}

void AInfiniteTerrainGameMode::PopulateBoundsVolumePool()
{
	auto VolumeIterator = TActorIterator<ANavMeshBoundsVolume>(GetWorld());

	while (VolumeIterator)
	{
		AddToPool(*VolumeIterator);
		++VolumeIterator;
	}
}

/*
UActorPool* AInfiniteTerrainGameMode::GetPool() const
{
	return NavMeshBoundsVolumePool;
}

void AInfiniteTerrainGameMode::SetPool(UActorPool* PoolToSet)
{
	NavMeshBoundsVolumePool = PoolToSet;
}
*/

void AInfiniteTerrainGameMode::AddToPool(ANavMeshBoundsVolume* VolumeToAdd)
{
	UE_LOG(LogTemp, Warning, TEXT("Added: %s"), *VolumeToAdd->GetName())
	NavMeshBoundsVolumePool->Add(VolumeToAdd);
}