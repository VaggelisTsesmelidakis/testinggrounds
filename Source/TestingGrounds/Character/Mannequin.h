// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Mannequin.generated.h"

class UCameraComponent;
class ASniperRifle;

UCLASS()
class TESTINGGROUNDS_API AMannequin : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMannequin();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void UnPossessed() override;

	UPROPERTY(EditDefaultsOnly, Category = "Guns")
	TSubclassOf<ASniperRifle> FPGunBlueprint;

	UPROPERTY(EditDefaultsOnly, Category = "Guns")
	TSubclassOf<ASniperRifle> TPGunBlueprint;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Firing")
	virtual void PullTrigger();

private:	
	UPROPERTY (VisibleDefaultsOnly, Category = "Mesh")
	UCameraComponent* FPCamera;

	UPROPERTY(VisibleDefaultsOnly, Category = "Mesh")
	USkeletalMeshComponent* FPArms;

	UPROPERTY(VisibleAnywhere, Category = "Guns")
	ASniperRifle* FPGun;

	UPROPERTY(VisibleAnywhere, Category = "Guns")
	ASniperRifle* TPGun;
	//TODO The two guns should be different; user should be able to choose different weapons for Player and AI

};
