// Fill out your copyright notice in the Description page of Project Settings.


#include "Mannequin.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Weapons\SniperRifle.h"
#include "Components\SkeletalMeshComponent.h"
#include "Components\InputComponent.h"


// Sets default values
AMannequin::AMannequin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a camera component
	FPCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FP Camera"));
	FPCamera->SetupAttachment(GetCapsuleComponent());
	FPCamera->RelativeLocation = FVector(-39.56f, 1.75f, 64.f);
	FPCamera->bUsePawnControlRotation = true;
	
	FPArms = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP Arms"));
	FPArms->SetupAttachment(FPCamera);
	FPArms->SetOnlyOwnerSee(true);
	FPArms->bCastDynamicShadow = false;
	FPArms->CastShadow = false;
	FPArms->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
	FPArms->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
}

// Called when the game starts or when spawned
void AMannequin::BeginPlay()
{
	Super::BeginPlay();

	if (ensure(FPGunBlueprint != nullptr && TPGunBlueprint != nullptr))
	{
		FPGun = GetWorld()->SpawnActor<ASniperRifle>(FPGunBlueprint);
		if (IsPlayerControlled())
		{
			// Attach gun to first person arms if Mannequin is controlled by Player...
			FPGun->AttachToComponent(FPArms, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
			
		}
		else
		{
			// ... otherwise attach to the standard Mesh of the Mannequin because it's controlled by AI
			FPGun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
			
		}
		
		FPGun->FirstPersonAnimInstance = FPArms->GetAnimInstance();
		FPGun->ThirdPersonAnimInstance = GetMesh()->GetAnimInstance();

		/* Third person gun implementation?
		TPGun = GetWorld()->SpawnActor<ASniperRifle>(TPGunBlueprint);
		TPGun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
		TPGun->AnimInstance = GetMesh()->GetAnimInstance();
	*/

		if (InputComponent != nullptr)
		{
			InputComponent->BindAction("Fire", IE_Pressed, this, &AMannequin::PullTrigger);
		}
	}
  
}

void AMannequin::PullTrigger()
{
	FPGun->OnFire();
}

// Called every frame
void AMannequin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMannequin::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMannequin::UnPossessed()
{
	Super::UnPossessed();
	if (FPGun)
	{
		FPGun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	}

}

