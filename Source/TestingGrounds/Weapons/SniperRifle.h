// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SniperRifle.generated.h"

UCLASS()
class TESTINGGROUNDS_API ASniperRifle : public AActor
{
	GENERATED_BODY()

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* KSR29_Rifle;

	/* Magazine mesh */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* KSR29_Magazine;

	/* Scope mesh */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* KSR29_Scope;

	/* Tripod mesh */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* KSR29_Tripod;

	/** Location on gun mesh where magazine should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* KSR29_MagazineLocation;

	/** Location on gun mesh where scope should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* KSR29_ScopeLocation;

	/** Location on gun mesh where tripod should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* KSR29_TripodLocation;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* KSR29_MuzzleLocation;

public:	
	// Sets default values for this actor's properties
	ASniperRifle();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Fires a projectile. */
	UFUNCTION(BlueprintCallable, Category = "Firing")
	void OnFire();

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector GunOffset;	// TODO Figure out if I still need this

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class ABallProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* FireSound;

	/** AnimMontages to play each time we fire; one for Player (first person arms) and one for AI character (third person arms) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimMontage* FirstPersonFireAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimMontage* ThirdPersonFireAnimation;

	UPROPERTY()
		class UAnimInstance* FirstPersonAnimInstance;

	UPROPERTY()
		class UAnimInstance* ThirdPersonAnimInstance;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
