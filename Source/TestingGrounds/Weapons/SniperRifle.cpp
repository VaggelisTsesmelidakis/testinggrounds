// Fill out your copyright notice in the Description page of Project Settings.


#include "SniperRifle.h"
#include "Components\SkeletalMeshComponent.h"
#include "BallProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "Animation/AnimInstance.h"

// Sets default values
ASniperRifle::ASniperRifle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a gun mesh component
	KSR29_Rifle = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("KSR29 Rifle"));
	//FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	KSR29_Rifle->bCastDynamicShadow = true;
	KSR29_Rifle->CastShadow = true;
	
	
	SetRootComponent(KSR29_Rifle);
	
	KSR29_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT(" MuzzleLocation"));
	KSR29_MuzzleLocation->SetupAttachment(KSR29_Rifle);
	KSR29_MuzzleLocation->SetRelativeLocation(FVector(505.0f, 1.4f, 45.4f));
	
	KSR29_Magazine = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("KSR29 Magazine"));
	KSR29_Magazine->SetupAttachment(KSR29_Rifle, TEXT("Magazine"));

	KSR29_Scope = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("KSR29 Scope"));
	KSR29_Scope->SetupAttachment(KSR29_Rifle, TEXT("Scope"));

	KSR29_Tripod = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("KSR29 Tripod"));
	KSR29_Tripod->SetupAttachment(KSR29_Rifle, TEXT("Tripod"));
	

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);
}

// Called when the game starts or when spawned
void ASniperRifle::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ASniperRifle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASniperRifle::OnFire()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (ensure(KSR29_MuzzleLocation != NULL))
			{
				const FRotator SpawnRotation = KSR29_MuzzleLocation->GetComponentRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = KSR29_MuzzleLocation->GetComponentLocation();

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<ABallProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified for first person
	if (FirstPersonFireAnimation != nullptr && FirstPersonAnimInstance != nullptr)
	{
		FirstPersonAnimInstance->Montage_Play(FirstPersonFireAnimation, 1.f);
	}

	if (ThirdPersonFireAnimation != nullptr && ThirdPersonAnimInstance != nullptr)
	{
		ThirdPersonAnimInstance->Montage_Play(ThirdPersonFireAnimation, 1.f);
	}
	
}
