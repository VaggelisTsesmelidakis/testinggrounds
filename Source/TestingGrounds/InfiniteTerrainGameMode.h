// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TestingGroundsGameMode.h"
#include "InfiniteTerrainGameMode.generated.h"

/**
 * Class to help spawn infinite terrain ("tiles") for the game, including NavMeshBoundsVolumes
 */

// Forward decleration
class ANavMeshBoundsVolume;
class UActorPool;

UCLASS()
class TESTINGGROUNDS_API AInfiniteTerrainGameMode : public ATestingGroundsGameMode
{
	GENERATED_BODY()

public:
	
	AInfiniteTerrainGameMode();

	UFUNCTION(BlueprintCallable, Category = "Actor Pools")
	void PopulateBoundsVolumePool();

/*
	UFUNCTION(BlueprintPure, Category = "Actor Pools")
	UActorPool* GetPool() const;

	UFUNCTION(BlueprintCallable, Category = "Actor Pools")
	void SetPool(UActorPool* PoolToSet);
*/

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Nav Mesh Pool")
	UActorPool* NavMeshBoundsVolumePool;

private:
	void AddToPool(ANavMeshBoundsVolume* VolumeToAdd);

};
